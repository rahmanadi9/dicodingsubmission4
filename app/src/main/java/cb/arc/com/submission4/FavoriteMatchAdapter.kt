package cb.arc.com.submission4

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cb.arc.com.submission4.db.FavoriteEvent
import kotlinx.android.synthetic.main.row_match.view.*

class FavoriteMatchAdapter(val favorites:List<FavoriteEvent>, val listener:(FavoriteEvent) -> Unit):
    RecyclerView.Adapter<FavoriteMatchAdapter.FavoriteMatchViewHolder>(){

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): FavoriteMatchViewHolder =
        FavoriteMatchViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.row_match,p0,false))

    override fun getItemCount(): Int = favorites.size

    override fun onBindViewHolder(p0: FavoriteMatchViewHolder, p1: Int) {
        p0.bindItem(favorites[p1],listener)
    }

    class FavoriteMatchViewHolder(view: View) : RecyclerView.ViewHolder(view){

        fun bindItem(favorite: FavoriteEvent,listener: (FavoriteEvent) -> Unit){
            itemView.tvDateMatch.text = favorite.dateEvent
            itemView.tvHome.text = favorite.strHomeTeam
            if(favorite.intHomeScore == null){
                itemView.tvScoreHome.text = " "
            }else{
                itemView.tvScoreHome.text = favorite.intHomeScore
            }
            itemView.tvAway.text = favorite.strAwayTeam
            if(favorite.intAwayScore == null ){
                itemView.tvScoreAway.text = " "
            }else{
                itemView.tvScoreAway.text = favorite.intAwayScore
            }

            itemView.cl.setOnClickListener {
                listener(favorite)
            }
        }
    }
}