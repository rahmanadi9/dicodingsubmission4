package cb.arc.com.submission4

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import cb.arc.com.submission4.R.id.*
import cb.arc.com.submission4.match.MatchFragmentFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        var mode = 0
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        navigation.setOnNavigationItemSelectedListener { item ->
            when(item.itemId){
                prev_match->{
                    mode = 0
                    loadMatchFragment(savedInstanceState)
                }
                next_matach->{
                    mode = 1
                    loadMatchFragment(savedInstanceState)
                }
                favorite->{
                    mode = 2
                    loadFavoriteFragment(savedInstanceState)
                }
            }
            true
        }
        navigation.selectedItemId = prev_match
    }

    private fun loadMatchFragment(savedInstanceState: Bundle?){
        if(savedInstanceState == null){
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.fl,MatchFragmentFragment(),MatchFragmentFragment::class.java.simpleName)
                .commit()
        }
    }

    private fun loadFavoriteFragment(savedInstanceState: Bundle?){
        if(savedInstanceState == null){
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.fl,FavoriteMatchFragment(),FavoriteMatchFragment::class.java.simpleName)
                .commit()
        }
    }
}
