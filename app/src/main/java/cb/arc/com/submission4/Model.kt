package cb.arc.com.submission4

object Model {
    data class result(val events: List<event>)
    data class event(val idEvent: String,
                     val strHomeTeam: String,
                     val strAwayTeam: String,
                     val intHomeScore: String,
                     val intAwayScore: String,
                     val idHomeTeam: String,
                     val idAwayTeam: String,
                     val dateEvent:String,
                     val strHomeGoalDetails:String,
                     val strAwayGoalDetails:String,
                     val intHomeShots:String,
                     val intAwayShots:String,
                     val strHomeLineupGoalkeeper:String,
                     val strAwayLineupGoalkeeper:String,
                     val strHomeLineupDefense:String,
                     val strAwayLineupDefense:String,
                     val strHomeLineupMidfield:String,
                     val strAwayLineupMidfield:String,
                     val strHomeLineupForward:String,
                     val strAwayLineupForward:String)

    data class detailTeam(val teams: List<team>)
    data class team(val strTeamBadge:String,
                    val strTeam:String)
}