package cb.arc.com.submission4

import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface SportDBService {

    @GET("eventspastleague.php")
    fun getPastEvents(@Query("id") id:String) : Observable<Model.result>

    @GET("eventsnextleague.php")
    fun getNextEvents(@Query("id") id:String) : Observable<Model.result>

    @GET("lookupteam.php")
    fun getDetailTeam(@Query("id") id:String): Observable<Model.detailTeam>

    @GET("lookupevent.php")
    fun getDetailEvent(@Query("id") id:String): Observable<Model.result>

    companion object {
        fun create():SportDBService{

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://www.thesportsdb.com/api/v1/json/1/")
                .build()

            return retrofit.create(SportDBService::class.java)
        }
    }
}