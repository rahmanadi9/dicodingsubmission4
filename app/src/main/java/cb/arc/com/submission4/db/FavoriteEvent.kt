package cb.arc.com.submission4.db

data class FavoriteEvent(val id:Long?,
                         val idEvent:String?,
                         val strHomeBadge:String?,
                         val strAwayBadge:String?,
                         val strHomeTeam:String?,
                         val strAwayTeam:String?,
                         val intHomeScore:String?,
                         val intAwayScore:String?,
                         val idHomeTeam:String?,
                         val idAwayTeam:String?,
                         val dateEvent:String?,
                         val strHomeGoalDetails:String?,
                         val strAwayGoalDetails:String?,
                         val intHomeShots:String?,
                         val intAwayShots:String?,
                         val strHomeLineupGoalkeeper:String?,
                         val strAwayLineupGoalkeeper:String?,
                         val strHomeLineupDefense:String?,
                         val strAwayLineupDefense:String?,
                         val strHomeLineupMidfield:String?,
                         val strAwayLineupMidfield:String?,
                         val strHomeLineupForward:String?,
                         val strAwayLineupForward:String?) {

    companion object {
        const val TABLE_FAVORITE: String = "TABLE_FAVORITE"
        const val ID: String="ID"
        const val EVENT_ID: String = "EVENT_ID"
        const val HOME_BADGE: String = "HOME_BADGE"
        const val AWAY_BADGE: String = "AWAY_BADGE"
        const val HOME_TEAM: String = "HOME_TEAM"
        const val AWAY_TEAM: String = "AWAY_TEAM"
        const val HOME_SCORE: String = "HOME_SCORE"
        const val AWAY_SCORE: String = "AWAY_SCORE"
        const val HOME_ID: String = "HOME_ID"
        const val AWAY_ID: String = "AWAY_ID"
        const val DATE_EVENT: String = "DATE_EVENT"
        const val HOME_GOAL_DETAILS: String = "HOME_GOAL_DETAILS"
        const val AWAY_GOAL_DETAILS: String = "AWAY_GOAL_DETAILS"
        const val HOME_SHOTS: String = "HOME_SHOTS"
        const val AWAY_SHOTS: String = "AWAY_SHOTS"
        const val HOME_LINE_UP_GOAL_KEEPER: String = "HOME_LINE_UP_GOAL_KEEPER"
        const val AWAY_LINE_UP_GOAL_KEEPER: String = "AWAY_LINE_UP_GOAL_KEEPER"
        const val HOME_LINE_UP_DEFENSE: String = "HOME_LINE_UP_DEFENSE"
        const val AWAY_LINE_UP_DEFENSE: String = "AWAY_LINE_UP_DEFENSE"
        const val HOME_LINE_UP_MID_FIELD: String = "HOME_LINE_UP_MID_FIELD"
        const val AWAY_LINE_UP_MID_FIELD: String = "AWAY_LINE_UP_MID_FIELD"
        const val HOME_LINE_UP_FORWARD: String = "HOME_LINE_UP_FORWARD"
        const val AWAY_LINE_UP_FORWARD: String = "AWAY_LINE_UP_FORWARD"
    }
}