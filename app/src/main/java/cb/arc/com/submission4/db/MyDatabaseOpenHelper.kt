package cb.arc.com.submission4.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

class MyDatabaseOpenHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx,"FavoriteEvent.db",null,1){
    companion object {
        private var instance: MyDatabaseOpenHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): MyDatabaseOpenHelper {
            if(instance == null){
                instance =
                        MyDatabaseOpenHelper(ctx.applicationContext)
            }
            return instance as MyDatabaseOpenHelper
        }
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.createTable(
            FavoriteEvent.TABLE_FAVORITE,true,
                FavoriteEvent.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                        FavoriteEvent.EVENT_ID to TEXT,
                        FavoriteEvent.HOME_BADGE to TEXT,
                        FavoriteEvent.AWAY_BADGE to TEXT,
                        FavoriteEvent.HOME_TEAM to TEXT,
                        FavoriteEvent.AWAY_TEAM to TEXT,
                        FavoriteEvent.HOME_SCORE to TEXT,
                        FavoriteEvent.AWAY_SCORE to TEXT,
                        FavoriteEvent.HOME_ID to TEXT,
                        FavoriteEvent.AWAY_ID to TEXT,
                        FavoriteEvent.DATE_EVENT to TEXT,
                        FavoriteEvent.HOME_GOAL_DETAILS to TEXT,
                        FavoriteEvent.AWAY_GOAL_DETAILS to TEXT,
                        FavoriteEvent.HOME_SHOTS to TEXT,
                        FavoriteEvent.AWAY_SHOTS to TEXT,
                        FavoriteEvent.HOME_LINE_UP_GOAL_KEEPER to TEXT,
                        FavoriteEvent.AWAY_LINE_UP_GOAL_KEEPER to TEXT,
                        FavoriteEvent.HOME_LINE_UP_DEFENSE to TEXT,
                        FavoriteEvent.AWAY_LINE_UP_DEFENSE to TEXT,
                        FavoriteEvent.HOME_LINE_UP_MID_FIELD to TEXT,
                        FavoriteEvent.AWAY_LINE_UP_MID_FIELD to TEXT,
                        FavoriteEvent.HOME_LINE_UP_FORWARD to TEXT,
                        FavoriteEvent.AWAY_LINE_UP_FORWARD to TEXT
        )
    }

    override fun onUpgrade(db: SQLiteDatabase?, p1: Int, p2: Int) {
        db?.dropTable(FavoriteEvent.TABLE_FAVORITE,true)
    }
}

val Context.database: MyDatabaseOpenHelper
    get() = MyDatabaseOpenHelper.getInstance(applicationContext)