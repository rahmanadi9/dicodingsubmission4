package cb.arc.com.submission4.detail

import android.database.sqlite.SQLiteConstraintException
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import cb.arc.com.submission4.*
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_detail.*
import cb.arc.com.submission4.R.menu.detail_menu
import cb.arc.com.submission4.R.drawable.ic_added_to_favorites
import cb.arc.com.submission4.R.drawable.ic_add_to_favorites
import cb.arc.com.submission4.R.id.add_to_favorite
import cb.arc.com.submission4.db.FavoriteEvent
import cb.arc.com.submission4.db.database
import org.jetbrains.anko.ctx
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.toast

class DetailActivity : AppCompatActivity(),DetailView {

    var idEvent:String =""
    var idHome:String = ""
    var idAway:String = ""

    lateinit var homeTeam:Model.team
    lateinit var awayTeam:Model.team
    lateinit var event:Model.event

    var dateEvent:String = ""
    var isNext: Boolean = false

    lateinit var detailPresenter: DetailPresenter

    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false
    private lateinit var id: String

    private var favorites:MutableList<FavoriteEvent> = mutableListOf()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val intent = intent
        if(intent.getStringExtra("id") == null){
            // from api
            idEvent = intent.getStringExtra("idEvent")
            idHome = intent.getStringExtra("idHome")
            idAway = intent.getStringExtra("idAway")
            dateEvent = intent.getStringExtra("dateEvent")
            isNext = intent.getBooleanExtra("isNext",false)
            tvMatchDate.text = dateEvent

            detailPresenter = DetailPresenterImpl()
            detailPresenter.setView(this)

            detailPresenter.loadHomeData(idHome)
            detailPresenter.loadAwayData(idAway)

            if(MainActivity.mode == 0){
                detailPresenter.loadEventData(idEvent)
            }
        }else{
            id = intent.getStringExtra("id")
            showFromDB(id)
        }

    }

    fun showFromDB(id:String){
        database?.use {
            val result = select(FavoriteEvent.TABLE_FAVORITE)
                .whereArgs(FavoriteEvent.EVENT_ID +" = {EVENT_ID} ","EVENT_ID" to id)
            val favorites = result.parseList(classParser<FavoriteEvent>())

            //image
            Glide.with(ctx).load(favorites.get(0).strHomeBadge).into(ivHome)
            Glide.with(ctx).load(favorites.get(0).strAwayBadge).into(ivAway)

            tvNamaHomeTeam.text = favorites.get(0).strHomeTeam
            tvNamaAwayTeam.text = favorites.get(0).strAwayTeam

            tvMatchDate.text = favorites.get(0).dateEvent

            tvScoreDetailHome.text = favorites.get(0).intHomeScore
            tvScoreDetailHome.text = favorites.get(0).intAwayScore

            tvGoalDetailHome.text = favorites.get(0).strHomeGoalDetails
            tvGoalDetailAway.text = favorites.get(0).strAwayGoalDetails

            tvHomeShot.text = favorites.get(0).intHomeShots
            tvAwayShot.text = favorites.get(0).intAwayShots

            tvHomeKeeper.text = favorites.get(0).strHomeLineupGoalkeeper
            tvAwayKeeper.text = favorites.get(0).strAwayLineupGoalkeeper

            tvHomeDefense.text = favorites.get(0).strHomeLineupDefense
            tvAwayDefense.text = favorites.get(0).strAwayLineupDefense

            tvHomeMidField.text = favorites.get(0).strHomeLineupMidfield
            tvAwayMidField.text = favorites.get(0).strAwayLineupMidfield

            tvHomeForward.text = favorites.get(0).strHomeLineupForward
            tvAwayForward.text = favorites.get(0).strAwayLineupForward
        }
        isFavorite = true
    }

    override fun onSuccessLoadHomeTeam(home: Model.team) {
        homeTeam = home
        Glide.with(this).load(home.strTeamBadge).into(ivHome)
        tvNamaHomeTeam.text = home.strTeam
    }

    override fun onSuccessLoadAwayTeam(away: Model.team) {
        awayTeam = away
        Glide.with(this).load(away.strTeamBadge).into(ivAway)
        tvNamaAwayTeam.text = away.strTeam
    }

    override fun onSuccessLoadDetail(event: Model.event) {
        this.event = event
        tvScoreDetailHome.text = event.intHomeScore
        tvScoreDetailAway.text = event.intAwayScore

        tvGoalDetailHome.text = event.strHomeGoalDetails
        tvGoalDetailAway.text = event.strAwayGoalDetails

        tvHomeShot.text = event.intHomeShots
        tvAwayShot.text = event.intAwayShots

        tvHomeKeeper.text = event.strHomeLineupGoalkeeper
        tvAwayKeeper.text = event.strAwayLineupGoalkeeper

        tvHomeDefense.text = event.strHomeLineupDefense
        tvAwayDefense.text = event.strAwayLineupDefense

        tvHomeMidField.text = event.strHomeLineupMidfield
        tvAwayMidField.text = event.strAwayLineupMidfield

        tvHomeForward.text = event.strHomeLineupForward
        tvAwayForward.text = event.strAwayLineupForward
    }

    override fun onFailedLoadDetail(message: String) {
        toast(message)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(detail_menu,menu)
        menuItem = menu
        setFavorite()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when(item?.itemId){
            android.R.id.home ->{
                finish()
                true
            }
            add_to_favorite ->{
                if(isFavorite) removeFromFavorite() else addToFavorite()

                isFavorite = !isFavorite
                setFavorite()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun addToFavorite(){
        try{
            database.use {
                insert(
                    FavoriteEvent.TABLE_FAVORITE,
                        FavoriteEvent.EVENT_ID to event.idEvent,
                            FavoriteEvent.HOME_BADGE to homeTeam.strTeamBadge,
                            FavoriteEvent.AWAY_BADGE to awayTeam.strTeamBadge,
                            FavoriteEvent.HOME_TEAM to event.strHomeTeam,
                            FavoriteEvent.AWAY_TEAM to event.strAwayTeam,
                            FavoriteEvent.HOME_SCORE to event.intHomeScore,
                            FavoriteEvent.AWAY_SCORE to event.intAwayScore,
                            FavoriteEvent.HOME_ID to event.idHomeTeam,
                            FavoriteEvent.AWAY_ID to event.idAwayTeam,
                            FavoriteEvent.DATE_EVENT to event.dateEvent,
                            FavoriteEvent.HOME_GOAL_DETAILS to event.strHomeGoalDetails,
                            FavoriteEvent.AWAY_GOAL_DETAILS to event.strAwayGoalDetails,
                            FavoriteEvent.HOME_SHOTS to event.intHomeShots,
                            FavoriteEvent.AWAY_SHOTS to event.intAwayShots,
                            FavoriteEvent.HOME_LINE_UP_GOAL_KEEPER to event.strHomeLineupGoalkeeper,
                            FavoriteEvent.AWAY_LINE_UP_GOAL_KEEPER to event.strAwayLineupGoalkeeper,
                            FavoriteEvent.HOME_LINE_UP_DEFENSE to event.strHomeLineupDefense,
                            FavoriteEvent.AWAY_LINE_UP_DEFENSE to event.strAwayLineupDefense,
                            FavoriteEvent.HOME_LINE_UP_MID_FIELD to event.strHomeLineupMidfield,
                            FavoriteEvent.AWAY_LINE_UP_MID_FIELD to event.strAwayLineupMidfield,
                            FavoriteEvent.HOME_LINE_UP_FORWARD to event.strHomeLineupForward,
                            FavoriteEvent.AWAY_LINE_UP_FORWARD to event.strAwayLineupForward)
            }
            Toast.makeText(ctx,"Add To Favorites",Toast.LENGTH_SHORT).show()
        }catch (e: SQLiteConstraintException){
            Toast.makeText(ctx,e.localizedMessage,Toast.LENGTH_SHORT).show()
        }
    }

    private fun removeFromFavorite(){
        try{
            database.use {
                delete(
                    FavoriteEvent.TABLE_FAVORITE,"EVENT_ID = {id}",
                    "id" to id)
            }
            Toast.makeText(ctx,"remove from favorite",Toast.LENGTH_SHORT).show()
            finish()
        }catch (e: SQLiteConstraintException){
            Toast.makeText(ctx,e.localizedMessage,Toast.LENGTH_SHORT).show()
        }
    }

    private fun setFavorite(){
        if(isFavorite)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_added_to_favorites)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_add_to_favorites)
    }
}
