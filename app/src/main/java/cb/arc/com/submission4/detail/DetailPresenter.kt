package cb.arc.com.submission4.detail

interface DetailPresenter {
    fun setView(view: DetailView)
    fun loadHomeData(idHome:String)
    fun loadAwayData(idAway:String)
    fun loadEventData(idEvent:String)
}