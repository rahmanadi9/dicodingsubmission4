package cb.arc.com.submission4.detail

import cb.arc.com.submission4.SportDBService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class DetailPresenterImpl :DetailPresenter {
    private lateinit var view:DetailView

    private var disposable:Disposable? = null

    private val sportDBService by lazy{
        SportDBService.create()
    }

    override fun setView(view: DetailView) {
        this.view = view
    }

    override fun loadHomeData(idHome: String) {
        disposable = sportDBService.getDetailTeam(idHome)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {result -> view.onSuccessLoadHomeTeam(result.teams.get(0))}

            )
    }

    override fun loadAwayData(idAway: String) {
        disposable = sportDBService.getDetailTeam(idAway)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result -> view.onSuccessLoadAwayTeam(result.teams.get(0))}
            )
    }

    override fun loadEventData(idEvent: String) {
        disposable = sportDBService.getDetailEvent(idEvent)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {result -> view.onSuccessLoadDetail(result.events.get(0))}
            )
    }
}