package cb.arc.com.submission4.detail

import cb.arc.com.submission4.Model

interface DetailView {
    fun onSuccessLoadHomeTeam(home:Model.team)

    fun onSuccessLoadAwayTeam(away:Model.team)

    fun onSuccessLoadDetail(event: Model.event)
    fun onFailedLoadDetail(message:String)
}