package cb.arc.com.submission4.match

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cb.arc.com.submission4.Model
import cb.arc.com.submission4.R
import kotlinx.android.synthetic.main.row_match.view.*

class MatchAdapter(val events: List<Model.event>, val listener: (Model.event) -> Unit):
    RecyclerView.Adapter<MatchAdapter.MatchViewHolder>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MatchViewHolder =
        MatchViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.row_match,p0,false))

    override fun getItemCount(): Int = events.size

    override fun onBindViewHolder(p0: MatchViewHolder, p1: Int) {
        p0.bindItem(events[p1],listener)
    }


    class MatchViewHolder(view: View) : RecyclerView.ViewHolder(view){

        fun bindItem(event: Model.event, listener: (Model.event) -> Unit){
            itemView.tvDateMatch.text = event.dateEvent
            itemView.tvHome.text = event.strHomeTeam
            if(event.intHomeScore == null){
                itemView.tvScoreHome.text = " "
            }else{
                itemView.tvScoreHome.text = event.intHomeScore
            }
            itemView.tvAway.text = event.strAwayTeam
            if(event.intAwayScore == null){
                itemView.tvScoreAway.text = " "
            }else{
                itemView.tvScoreAway.text = event.intAwayScore
            }

            itemView.cl.setOnClickListener {
                listener(event)
            }
        }
    }
}