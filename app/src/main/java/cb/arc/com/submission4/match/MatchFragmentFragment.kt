package cb.arc.com.submission4.match


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import cb.arc.com.submission4.detail.DetailActivity
import cb.arc.com.submission4.MainActivity
import cb.arc.com.submission4.Model
import cb.arc.com.submission4.R
import kotlinx.android.synthetic.main.fragment_match.*
import org.jetbrains.anko.support.v4.startActivity


class MatchFragmentFragment : Fragment(),MatchView {


    var isPrev = true;
    lateinit var matchPresenter:MatchPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_match, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        matchPresenter = MatchPresenterImpl()
        matchPresenter.setView(this)

        matchPresenter.loadMatch(MainActivity.mode)

    }


    override fun onSuccessLoadMatch(events: List<Model.event>) {
//        if(rvMatch == null){
//           val rvMatch = find<RecyclerView>(R.id.rvMatch)
//        }
        rvMatch?.layoutManager = LinearLayoutManager(activity)
        rvMatch?.adapter = MatchAdapter(events){
            startActivity<DetailActivity>("idEvent" to it.idEvent,
                                            "idHome" to it.idHomeTeam,
                                            "idAway" to it.idAwayTeam,
                                            "dateEvent" to it.dateEvent,
                                            "isNext" to false)
        }
    }

    override fun onFailedLoadMatch(message: String) {
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show()
    }
}
