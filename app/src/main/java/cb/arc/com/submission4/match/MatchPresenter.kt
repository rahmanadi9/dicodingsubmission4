package cb.arc.com.submission4.match

interface MatchPresenter {
    fun setView(view:MatchView)
    fun loadMatch(mode:Int)
}