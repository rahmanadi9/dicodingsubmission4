package cb.arc.com.submission4.match

import cb.arc.com.submission4.Model
import cb.arc.com.submission4.SportDBService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class MatchPresenterImpl : MatchPresenter{
    private lateinit var view:MatchView

    private var disposable:Disposable? = null

    private val sportDBService by lazy {
        SportDBService.create()
    }

    override fun setView(view: MatchView) {
        this.view = view
    }

    override fun loadMatch(mode: Int) {
        when(mode){
            0 -> getPrevMatch()
            1 -> getNextMatch()
        }
    }

    fun getPrevMatch(){
        disposable = sportDBService.getPastEvents("4328")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result:Model.result -> view.onSuccessLoadMatch(result.events)}
            )
    }

    fun getNextMatch(){
        disposable = sportDBService.getNextEvents("4328")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result:Model.result -> view.onSuccessLoadMatch(result.events)}
            )
    }

    fun dispose(){
        disposable?.dispose()
    }
}