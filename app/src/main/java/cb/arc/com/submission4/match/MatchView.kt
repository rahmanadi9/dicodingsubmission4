package cb.arc.com.submission4.match

import cb.arc.com.submission4.Model

interface MatchView {
    fun onSuccessLoadMatch(events:List<Model.event>)
    fun onFailedLoadMatch(message:String)
}