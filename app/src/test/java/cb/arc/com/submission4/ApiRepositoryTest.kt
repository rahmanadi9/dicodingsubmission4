package cb.arc.com.submission4

import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify

class ApiRepositoryTest {

    @Test
    fun testRepo(){
        val repo = mock(SportDBService::class.java)
        repo.getDetailEvent("4328")
        verify(repo).getDetailEvent("4328")
    }
}
